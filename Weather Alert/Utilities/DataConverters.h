//
//  DataConverters.h
//  Weather Alert
//
//  Created by Adam Stone on 06/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DataConverters : NSObject

+ (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;
+ (NSString *)cardinalDirectionFromDegrees:(float)degrees;

@end

NS_ASSUME_NONNULL_END
