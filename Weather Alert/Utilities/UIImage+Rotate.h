//
//  UIImage+Rotate.h
//  Weather Alert
//
//  Created by Adam Stone on 07/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIImage (Rotate)

- (UIImage *)rotateByDegrees:(CGFloat)degrees;

@end

