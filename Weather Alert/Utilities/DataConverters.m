//
//  DataConverters.m
//  Weather Alert
//
//  Created by Adam Stone on 06/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import "DataConverters.h"

@implementation DataConverters

//Convert to object from NSDictionarey Based on key
+ (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return ([object isEqual:[NSNull null]] || object == nil) ? nil : object;
}

+ (NSString *)cardinalDirectionFromDegrees:(float)degrees {
    //Possible Directions
    NSArray *directions = @[@"N", @"NNE", @"NE", @"ENE", @"E", @"ESE", @"SE", @"SSE", @"S", @"SSW", @"SW", @"WSW", @"W", @"WNW", @"NW", @"NNW"];
    //Convert degrees to appropriate index for cardinal direction array
    int i = (degrees + 11.25) / 22.5;
    return directions[i % 16];
}

@end
