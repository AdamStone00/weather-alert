//
//  NibNames.h
//  Weather Alert
//
//  Created by Adam Stone on 06/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#ifndef NibNames_h
#define NibNames_h

#define nibFavouriteLocationsView                              @"FavouriteLocationsView"
#define nibLocationTableViewCell                               @"LocationTableViewCell"
#define nibLocationForecastView                                @"LocationForecastView"
#define nibForecastTableViewCell                               @"ForecastTableViewCell"
#define nibForecastGroupHeaderCell                             @"ForecastGroupHeaderCell"

#endif /* NibNames_h */
