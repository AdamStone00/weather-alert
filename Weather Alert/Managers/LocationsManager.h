//
//  LocationsManager.h
//  Weather Alert
//
//  Created by Adam Stone on 06/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Location.h"

NS_ASSUME_NONNULL_BEGIN

@interface LocationsManager : NSObject

//Shared Instance
+ (id)sharedInstance;

//Data Stores
@property (nonatomic, retain) NSArray<Location*> *locations;

//Access
-(Location *)locationForIdentifier:(NSNumber *)identifier;

@end

NS_ASSUME_NONNULL_END
