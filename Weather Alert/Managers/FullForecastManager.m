//
//  FullForecastManager.m
//  Weather Alert
//
//  Created by Adam Stone on 07/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import "FullForecastManager.h"

@implementation FullForecastManager

#pragma mark - Constructors
- (instancetype)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

#pragma mark - Methods
-(void)loadFullForecastForLocation:(Location *)loc{
    
    //Call Service and Get Full Forecast
    [OpenWeatherServiceManager getFullForecastForIDLocation:loc.identifier withCompletion:^(NSArray<FullForecast *> * _Nonnull fullForecast, NSError * _Nonnull error) {
       
        if (error){
            //Error Occured so notify delegate
            if (self.fullForecastDelegate && [self.fullForecastDelegate respondsToSelector:@selector(didFailToLoadFullForecast)]){
                [self.fullForecastDelegate  didFailToLoadFullForecast];
            }
        }else{
            //Data loaded fine so store
            self->fullForecastArray = fullForecast;
            //Get Date Groupings and build array
            self.groupedForecasts = [self createdGroupedForecast];
            //Notify Delegate
            if (self.fullForecastDelegate && [self.fullForecastDelegate respondsToSelector:@selector(didLoadFullForecast)]){
                [self.fullForecastDelegate didLoadFullForecast];
            }
        }
    }];
    
}

-(NSArray *)getDateGroupings {
    //Sort the array into date order
    NSSortDescriptor *dateSort = [[NSSortDescriptor alloc] initWithKey:@"dateFrom" ascending:YES];
    NSMutableArray *dates = [[NSMutableArray alloc] init];
    //Create dateformatter to compare date part only
    NSDateFormatter *dateOnlyFormatter = [[NSDateFormatter alloc] init];
    [dateOnlyFormatter setDateFormat:@"dd/MM/yyyy"];
    //Loop through array to build up array of just dates
    for (FullForecast *forecast in [fullForecastArray sortedArrayUsingDescriptors:@[dateSort]]){
        [dates addObject:[dateOnlyFormatter stringFromDate:forecast.dateFrom]];
    }
    //Return unique set of ordered dates
    return [[NSOrderedSet orderedSetWithArray:dates] array];
}

-(NSArray<FullForecast*> *)getForecastsForDate:(NSString *)dateString {
    //Create dateformatter to convert string to date
    NSDateFormatter *dateOnlyFormatter = [[NSDateFormatter alloc] init];
    [dateOnlyFormatter setDateFormat:@"dd/MM/yyyy"];
    NSDate *date = [dateOnlyFormatter dateFromString:dateString];
    //Compare Components
    unsigned int flags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    //Get Calendar
    NSCalendar *cal = [NSCalendar currentCalendar];
    //Date Components
    NSDateComponents* components = [cal components:flags fromDate:date];
    //Start Date
    NSDate *startDate = [cal dateFromComponents:components];
    //End Date By Adding 1 Day
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = 1;
    NSDate *endDate = [cal dateByAddingComponents:dayComponent toDate:date options:0];
    //Create Predicate
    NSPredicate *datePredicate = [NSPredicate predicateWithFormat:@"SELF.dateFrom >= %@ AND SELF.dateFrom < %@", startDate, endDate];
    //Sort the array into date order
    NSSortDescriptor *dateSort = [[NSSortDescriptor alloc] initWithKey:@"dateFrom" ascending:YES];
    //Return Sorted and Filtered Array
    return [[fullForecastArray sortedArrayUsingDescriptors:@[dateSort]] filteredArrayUsingPredicate:datePredicate];
}

-(NSArray *)createdGroupedForecast{
    NSMutableArray *groupedForecast = [[NSMutableArray alloc] init];
    NSArray *dateGroupings = [self getDateGroupings];
    
    for (NSString *dateGroup in dateGroupings){
        [groupedForecast addObject:@{kDateGroupTitle:dateGroup, kDateGroupForecasts:[self getForecastsForDate:dateGroup]}];
    }
    
    return groupedForecast;
}

@end
