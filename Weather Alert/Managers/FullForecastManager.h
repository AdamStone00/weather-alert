//
//  FullForecastManager.h
//  Weather Alert
//
//  Created by Adam Stone on 07/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OpenWeatherServiceManager.h"
#import "Location.h"

NS_ASSUME_NONNULL_BEGIN

#define kDateGroupTitle                             @"date"
#define kDateGroupForecasts                         @"forecasts"

//Delegate Protocol
@protocol FullForecastManagerDelegate <NSObject>

@required

- (void) didFailToLoadFullForecast;
- (void) didLoadFullForecast;

@end

@interface FullForecastManager : NSObject
{
    //Internal Storage
    NSArray<FullForecast *>* fullForecastArray;
}

//Properties
@property (nonatomic, weak) id <FullForecastManagerDelegate> fullForecastDelegate;
@property (nonatomic, strong) NSArray *groupedForecasts;

//Methods
-(void)loadFullForecastForLocation:(Location *)loc;

@end

NS_ASSUME_NONNULL_END
