//
//  OpenWeatherServiceManager.h
//  Weather Alert
//
//  Created by Adam Stone on 06/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CurrentForecast.h"
#import "FullForecast.h"

NS_ASSUME_NONNULL_BEGIN

@interface OpenWeatherServiceManager : NSObject

+ (void) getCurrentForecastForIDLocation:(NSNumber *)identifier withCompletion:(void (^) (CurrentForecast* forecast, NSError *error))completionHandler;
+ (void) getFullForecastForIDLocation:(NSNumber *)identifier withCompletion:(void (^) (NSArray<FullForecast*>* fullForecast, NSError *error))completionHandler;

@end

NS_ASSUME_NONNULL_END
