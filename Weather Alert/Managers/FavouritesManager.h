//
//  FavouritesManager.h
//  Weather Alert
//
//  Created by Adam Stone on 06/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Location.h"

NS_ASSUME_NONNULL_BEGIN

@interface FavouritesManager : NSObject

//Shared Instance
+ (id)sharedInstance;

//Methods
-(void)toggleFavourite:(NSNumber *)identifier;
-(BOOL)isFavourite:(NSNumber *)identifier;

//Data Stores
@property (nonatomic, retain) NSArray *favouriteLocationIDs;
@property (nonatomic, retain) NSUserDefaults *uDefaults;

@end

NS_ASSUME_NONNULL_END
