//
//  OpenWeatherServiceManager.m
//  Weather Alert
//
//  Created by Adam Stone on 06/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import "OpenWeatherServiceManager.h"
#import "MainConfiguration.h"
#import "DataConverters.h"

@implementation OpenWeatherServiceManager

#pragma mark - Current Forecast
+ (void) getCurrentForecastForIDLocation:(NSNumber *)identifier withCompletion:(void (^) (CurrentForecast* forecast, NSError *error))completionHandler{
    
    //Define Endpoint name
    NSString *endpoint = @"weather";
    
    //Define Search Query
    NSString *query = [NSString stringWithFormat:@"?id=%@&appid=%@", identifier, apiKey];
    
    //Debug Log Info
    if (isDebugMode){
        [self logCallForEndpoint:[NSString stringWithFormat:@"%@%@",endpoint,query]];
    }
    
    //Call Endpoint
    [self callEndpoint:[NSString stringWithFormat:@"%@%@",endpoint, query] completionHandler:^(NSData *result, NSError *error) {
        if (error){
            //Return Error If Occured
            completionHandler(nil, [self errorEndpoint:endpoint withErrorCode:idServiceError]);
        }else{
            //Define Return Object
            CurrentForecast *forecast = [[CurrentForecast alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:result options:0 error:nil]];
            //Return Result
            completionHandler(forecast, nil);
        }
    }];
    
}

#pragma mark - Full Forecast
+ (void) getFullForecastForIDLocation:(NSNumber *)identifier withCompletion:(void (^) (NSArray<FullForecast*>* fullForecast, NSError *error))completionHandler {
    
    //Define Endpoint name
    NSString *endpoint = @"forecast";
    
    //Define Search Query
    NSString *query = [NSString stringWithFormat:@"?id=%@&appid=%@", identifier, apiKey];
    
    //Debug Log Info
    if (isDebugMode){
        [self logCallForEndpoint:[NSString stringWithFormat:@"%@%@",endpoint,query]];
    }
    
    //Call Endpoint
    [self callEndpoint:[NSString stringWithFormat:@"%@%@",endpoint, query] completionHandler:^(NSData *result, NSError *error) {
        if (error){
            //Return Error If Occured
            completionHandler(nil, [self errorEndpoint:endpoint withErrorCode:idServiceError]);
        }else{
            //Define Return Object
            NSMutableArray *rtn = [[NSMutableArray alloc] init];
            //Unpack and Deserialise Result
            NSArray *forecastArray = [DataConverters objectOrNilForKey:@"list" fromDictionary:[NSJSONSerialization JSONObjectWithData:result options:0 error:nil]];
            //Unpack each return object into appropraite return type
            for (NSDictionary *dict in forecastArray){
                [rtn addObject:[[FullForecast alloc] initWithDictionary:dict]];
            }
            //Return Result
            completionHandler(rtn, nil);
        }
    }];
}

#pragma mark - Networking

+ (void) callEndpoint:(NSString*)endpoint completionHandler:(void (^) (NSData * result, NSError * error))completionHandler;
{
    @try {
        
        //Build URL based on base URL and endpoint
        NSURL *serviceURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", apiBaseURL, endpoint]];
        
        //Define request
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:serviceURL];
        //Set Verb
        [request setHTTPMethod:@"GET"];
        
        //Create new URL Session with Task for request
        [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            
            //Log Return if Debugging
            if (isDebugMode){
                NSLog(@"Endpoint: %@ | Response: %@", endpoint, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
            }
            
            //Return to caller
            completionHandler(data, error);
            
        }] resume];
        
    } @catch (NSException *exception) {
        //REturn error to caller
        completionHandler(nil, [self errorEndpoint:endpoint withErrorCode:idServiceError]);
    }
}

+(NSError *)errorEndpoint:(NSString *)endPoint withErrorCode:(int)errorCode{
    //Create error object
    NSDictionary *userInfo = @{NSLocalizedDescriptionKey: @"An Error Occured", NSLocalizedFailureReasonErrorKey: [NSString stringWithFormat:@"Unable to call Endpoit:%@", endPoint]};
    return [NSError errorWithDomain:errorDomain code:errorCode userInfo:userInfo];
}

#pragma mark - Logging

+(void)logCallForEndpoint:(NSString *)endpoint{
    //Log calling point for debugging
    NSLog(@"Calling Endpoint: %@", endpoint);
}

@end

