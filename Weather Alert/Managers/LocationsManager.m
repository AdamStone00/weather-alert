//
//  LocationsManager.m
//  Weather Alert
//
//  Created by Adam Stone on 06/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import "LocationsManager.h"

@implementation LocationsManager

#pragma mark - Constructors

//Create shared instance of object for use throughout app
+ (id)sharedInstance
{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
        [_sharedObject loadLocations];
    });
    return _sharedObject;
}

- (instancetype)init {
    self = [super init];
    return self;
}

#pragma mark - Load Locations

-(void)loadLocations {
    //Load Bundled JSON File
    NSString *locationsFilePath = [[NSBundle mainBundle] pathForResource:@"Locations" ofType:@"json"];
    NSData *locationData = [NSData dataWithContentsOfFile:locationsFilePath];
    
    //Serialise
    NSDictionary *locations = [NSJSONSerialization JSONObjectWithData:locationData options:kNilOptions error:nil];
    
    //Create objects and store
    NSMutableArray *locationsArray = [[NSMutableArray alloc] init];
    for (NSDictionary *loc in locations){
        [locationsArray addObject:[[Location alloc] initWithDictionary:loc]];
    }
    
    //Set objects to singleton store
    [self setLocations:locationsArray];
    
    //Tidy Up
    locations = nil;
    locationsArray = nil;
}

#pragma mark - Access
-(Location *)locationForIdentifier:(NSNumber *)identifier{
    NSPredicate *locPred = [NSPredicate predicateWithFormat:@"SELF.identifier == %@", identifier];
    NSArray *filtArray = [self.locations filteredArrayUsingPredicate:locPred];
    if ([filtArray count]){
        return [filtArray objectAtIndex:0];
    }else{
        return nil;
    }
}

@end
