//
//  FavouritesManager.m
//  Weather Alert
//
//  Created by Adam Stone on 06/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import "FavouritesManager.h"
#import "LocationsManager.h"

@implementation FavouritesManager

//Keys
NSString *const kUserFavouritesKey      =      @"userFavouriteLocations";

#pragma mark - Constructors

//Create shared instance of object for use throughout app
+ (id)sharedInstance
{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
        [_sharedObject loadFavourites];
    });
    return _sharedObject;
}

- (instancetype)init {
    self = [super init];
    return self;
}

#pragma mark - Load Lookup Favourites
- (void)loadFavourites {
    
    //Load NSUserDefaults
    self.uDefaults = [NSUserDefaults standardUserDefaults];
    
    //Load Stored Array of IDs
    [self setFavouriteLocationIDs:[self.uDefaults objectForKey:kUserFavouritesKey]];
    
}

#pragma mark - Add / Remove Favourite
-(void)toggleFavourite:(NSNumber *)identifier {
    NSMutableArray *favs = self.favouriteLocationIDs ? [self.favouriteLocationIDs mutableCopy] : [[NSMutableArray alloc] init];
    
    if ([self.favouriteLocationIDs containsObject:identifier]){
        [favs removeObject:identifier];
    }else{
        [favs addObject:identifier];
    }
    
    [self.uDefaults setObject:favs forKey:kUserFavouritesKey];
    [self loadFavourites];
}

#pragma mark - Check Favourite
- (BOOL)isFavourite:(NSNumber *)identifier {
    return [self.favouriteLocationIDs containsObject:identifier];
}

@end
