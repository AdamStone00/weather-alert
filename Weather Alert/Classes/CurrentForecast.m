//
//  CurrentForecast.m
//  Weather Alert
//
//  Created by Adam Stone on 06/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import "CurrentForecast.h"
#import "DataConverters.h"

@implementation CurrentForecast

//API Keys
NSString *const kForecastWind                 =      @"wind";
NSString *const KForecastWindSpeed            =      @"speed";
NSString *const kForecastWindDeg              =      @"deg";

@synthesize speed;
@synthesize degrees;

//Init
- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        [self setValuesWithDictionary:dict];
    }
    return self;
}

//Create object based on dictionary
-(void)setValuesWithDictionary:(NSDictionary *)dict
{
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        //Set Values
        
        NSDictionary *wind = [DataConverters objectOrNilForKey:kForecastWind fromDictionary:dict];
        if (wind){
            self.speed = [[DataConverters objectOrNilForKey:KForecastWindSpeed fromDictionary:wind] doubleValue];
            self.degrees = [[DataConverters objectOrNilForKey:kForecastWindDeg fromDictionary:wind] doubleValue];
        }
        
    }
}

- (NSString *)cardinalDirection {
    return [DataConverters cardinalDirectionFromDegrees:self.degrees];
}

@end
