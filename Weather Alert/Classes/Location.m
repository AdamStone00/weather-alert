//
//  Location.m
//  Weather Alert
//
//  Created by Adam Stone on 06/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import "Location.h"
#import "DataConverters.h"

@implementation Location

//API Keys
NSString *const kLocationID                 =      @"id";
NSString *const kLocationName               =      @"name";
NSString *const kLocationCountryCode        =      @"country";

@synthesize identifier;
@synthesize name;
@synthesize countryCode;

//Init
- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        [self setValuesWithDictionary:dict];
    }
    return self;
}

//Create object based on dictionary
-(void)setValuesWithDictionary:(NSDictionary *)dict
{
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        //Set Values
        self.identifier = [DataConverters objectOrNilForKey:kLocationID fromDictionary:dict];
        self.name = [DataConverters objectOrNilForKey:kLocationName fromDictionary:dict];
        self.countryCode =  [DataConverters objectOrNilForKey:kLocationCountryCode fromDictionary:dict];
    }
}

@end
