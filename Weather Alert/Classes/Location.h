//
//  Location.h
//  Weather Alert
//
//  Created by Adam Stone on 06/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Location : NSObject

//Properties
@property (nonatomic, copy, nullable)       NSNumber *identifier;
@property (nonatomic, copy, nullable)       NSString *name;
@property (nonatomic, copy, nullable)       NSString *countryCode;

//Creation
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (void)setValuesWithDictionary:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
