//
//  FullForecast.h
//  Weather Alert
//
//  Created by Adam Stone on 06/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FullForecast : NSObject

//Properties
@property (nonatomic, assign) double speed;
@property (nonatomic, assign) double degrees;
@property (nonatomic, strong) NSDate *dateFrom;

//Creation
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (void)setValuesWithDictionary:(NSDictionary *)dict;

//Ready Only
- (NSString *)cardinalDirection;

@end

NS_ASSUME_NONNULL_END
