//
//  FullForecast.m
//  Weather Alert
//
//  Created by Adam Stone on 06/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import "FullForecast.h"
#import "DataConverters.h"

@implementation FullForecast

//API Keys
NSString *const kFullForecastWind                 =      @"wind";
NSString *const KFullForecastWindSpeed            =      @"speed";
NSString *const kFullForecastWindDeg              =      @"deg";
NSString *const kFullForecastDateFrom             =      @"dt";

@synthesize speed;
@synthesize degrees;
@synthesize dateFrom;

//Init
- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        [self setValuesWithDictionary:dict];
    }
    return self;
}

//Create object based on dictionary
-(void)setValuesWithDictionary:(NSDictionary *)dict
{
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        //Set Values
        NSDictionary *wind = [DataConverters objectOrNilForKey:kFullForecastWind fromDictionary:dict];
        if (wind){
            self.speed = [[DataConverters objectOrNilForKey:KFullForecastWindSpeed fromDictionary:wind] doubleValue];
            self.degrees = [[DataConverters objectOrNilForKey:kFullForecastWindDeg fromDictionary:wind] doubleValue];
        }
        NSInteger dateFromUTC = [[DataConverters objectOrNilForKey:kFullForecastDateFrom fromDictionary:dict] integerValue];
        self.dateFrom = [[NSDate alloc] initWithTimeIntervalSince1970:dateFromUTC];
    }
}

- (NSString *)cardinalDirection {
    return [DataConverters cardinalDirectionFromDegrees:self.degrees];
}

@end
