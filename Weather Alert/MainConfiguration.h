//
//  MainConfiguration.h
//  Weather Alert
//
//  Created by Adam Stone on 06/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#ifndef MainConfiguration_h
#define MainConfiguration_h

#define apiBaseURL                          @"https://api.openweathermap.org/data/2.5/"
#define apiKey                              @"718c52b2df91b6c6f49a8b8f0e8c6db1"
#define isDebugMode                         YES //If yes the app will log all web calls / returns in the output window
#define appVersion                          [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]
#define errorDomain                         @"com.AdamStone.WeatherAlert"
#define idServiceError                      9001
#define speedUnit                          @"mph"

#endif /* MainConfiguration_h */
