//
//  AppDelegate.m
//  Weather Alert
//
//  Created by Adam Stone on 06/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import "AppDelegate.h"
#import "FavouriteLocationsViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    //Create first view and load
    [self setUpInitialView];
    return YES;
}

-(void)setUpInitialView{
    //UI So run on main thread
    dispatch_async( dispatch_get_main_queue(), ^{
        //Get window
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        //Create navigation controller and initial controller
        FavouriteLocationsViewController *favouriteLocationsView = [[FavouriteLocationsViewController alloc] init];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:favouriteLocationsView];
        [navController setNavigationBarHidden:YES];
        //Assign navigation controller as root view
        self.window.rootViewController = navController;
        //Launch
        [self.window makeKeyAndVisible];
    });
}


@end
