//
//  LocationForecastViewController.h
//  Weather Alert
//
//  Created by Adam Stone on 07/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Location.h"
#import "FullForecastManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface LocationForecastViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, FullForecastManagerDelegate>

//UI Elements
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentSpeed;
@property (weak, nonatomic) IBOutlet UILabel *lblCardinalDirection;
@property (weak, nonatomic) IBOutlet UILabel *lblLocationName;
@property (weak, nonatomic) IBOutlet UIView *vwLocationName;
@property (weak, nonatomic) IBOutlet UIImageView *imgCardinalDirection;
@property (weak, nonatomic) IBOutlet UITableView *tblFullForecast;
@property (weak, nonatomic) IBOutlet UIImageView *imgLocationIcon;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *aiLoading;

//Data
@property (strong, nonatomic) Location *location;
@property (strong, nonatomic) CurrentForecast *currentForecast;
@property (strong, nonatomic) NSArray<FullForecast *> *fullForecastArray;
@property (strong, nonatomic) FullForecastManager *forecastManager;

//Events
- (IBAction)btnBackTapped:(id)sender;



@end

NS_ASSUME_NONNULL_END
