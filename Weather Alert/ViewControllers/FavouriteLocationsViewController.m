//
//  FavouriteLocationsViewController.m
//  Weather Alert
//
//  Created by Adam Stone on 06/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import "FavouriteLocationsViewController.h"
#import "NibNames.h"
#import "LocationTableViewCell.h"
#import "FavouritesManager.h"
#import "OpenWeatherServiceManager.h"
#import "LocationForecastViewController.h"

@interface FavouriteLocationsViewController ()

@end

@implementation FavouriteLocationsViewController

@synthesize vwHeader;
@synthesize vwEmptyData;
@synthesize txtSearchBar;
@synthesize lblTitle;
@synthesize imgEmptyDataIcon;
@synthesize btnCancelSearch;
@synthesize tblSearchResults;
@synthesize tblFavouriteLocations;
@synthesize aiSearching;
@synthesize vwLoading;
@synthesize vwLoadingInner;

#pragma mark - View Load

- (id)init
{
    self = [self initWithNibName:nibFavouriteLocationsView bundle:nil];
    
    if (self) {
        [self setTitle:@""];
    };
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureViewStyle];
    [self configureTableStyle];
    [self configureViewState];
}

#pragma mark - View style

-(void)configureViewStyle{
    //Search Bar
    [[self.txtSearchBar layer] setCornerRadius:4.0f];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, self.txtSearchBar.frame.size.height)];
    [self.txtSearchBar setLeftView:paddingView];
    [self.txtSearchBar setLeftViewMode:UITextFieldViewModeAlways];
    
    //Icon
    [self.imgEmptyDataIcon setTintColor:[UIColor whiteColor]];
    
    //LoadingView
    [[self.vwLoadingInner layer] setCornerRadius:8.0f];
}

#pragma mark - Configure Current View State {
- (void)configureViewState{
    //Check to see if we have any data, if not show the empty data view
    if ([[[FavouritesManager sharedInstance] favouriteLocationIDs] count]) {
        [self.vwEmptyData setAlpha:0.0f];
        [self.tblFavouriteLocations setAlpha:1.0f];
    }else{
        [self.vwEmptyData setAlpha:1.0f];
        [self.tblFavouriteLocations setAlpha:0.0f];
    }
}

#pragma mark - View Transitions

- (IBAction)txtSearchBarEditingDidBegin:(id)sender {
    //Animate Transition To Search Mode
    [UIView animateWithDuration:0.3 animations:^{
        [self.cHeaderHeight setConstant:80];
        [self.cSearchBarTrailing setConstant:45];
        [self.btnCancelSearch setAlpha:1.0f];
        [self.vwEmptyData setAlpha:0.0f];
        [self.lblTitle setAlpha:0.0f];
        [self.tblSearchResults setAlpha:1.0f];
        [self.tblFavouriteLocations setAlpha:0.0f];
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self.btnCancelSearch setImage:[UIImage imageNamed:@"CancelIcon"] forState:UIControlStateNormal];
    }];
}

- (IBAction)btnCancelSearchTapped:(id)sender {
    //End Editing and Transition
    [self dismissSearch];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    //If it's the search bar then just hide the keyboard, searching is being taken care of elsewhere
    if (textField == self.txtSearchBar){
        [self.txtSearchBar resignFirstResponder];
        return NO;
    }
    return YES;
}

-(void)dismissSearch{
    //Clear First Responder
    [self.txtSearchBar resignFirstResponder];
    //Invalidate Any Further Search
    [self.searchDelayTimer invalidate];
    //Clear UI
    [self.txtSearchBar setText:nil];
    [self.btnCancelSearch setImage:nil forState:UIControlStateNormal];
    [self setSearchResults:nil];
    [self.tblSearchResults reloadData];
    //Update Favourites Table
    [self.tblFavouriteLocations reloadData];
    //Animate Transition To Favourites Mode
    [UIView animateWithDuration:0.3 animations:^{
        [self.aiSearching setAlpha:0.0f];
        [self.cHeaderHeight setConstant:200];
        [self.cSearchBarTrailing setConstant:25];
        [self.btnCancelSearch setAlpha:0.0f];
        [[[FavouritesManager sharedInstance] favouriteLocationIDs] count] ? [self.tblFavouriteLocations setAlpha:1.0f] : [self.vwEmptyData setAlpha:1.0f];
        [self.lblTitle setAlpha:1.0f];
        [self.tblSearchResults setAlpha:0.0f];
        [self.view layoutIfNeeded];
    } completion:nil];
}


#pragma mark - Searching
- (IBAction)txtSearchBarEditingChanged:(id)sender {
    //Capture Search Term
    NSString *searchTerm = [self.txtSearchBar.text lowercaseString];
    
    //Start timer to wait for entry to pause / end
    [self.searchDelayTimer invalidate];
    [self setSearchDelayTimer:[NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(performSearch:) userInfo:searchTerm repeats:NO]];
    
    //Animate transition to loading results
    [UIView animateWithDuration:0.2 animations:^{
        [self.tblSearchResults setAlpha:0.2f];
        [self.aiSearching setAlpha:1.0f];
    } completion:^(BOOL finished) {
        [self.tblSearchResults setUserInteractionEnabled:NO];
    }];
    
}

- (void)performSearch:(NSTimer *)timer
{
    //Access search term
    NSString *searchTerm = timer.userInfo;
    
    //Async load
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        //Get matching locations
        NSMutableArray<Location *> *foundMatches = [NSMutableArray array];
        [[[LocationsManager sharedInstance] locations] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            Location *loc = (Location *)obj;
            if ([[loc.name lowercaseString] hasPrefix:searchTerm]){
                [foundMatches addObject:loc];
            }
        }];
        
        //Set Search Results
        [self setSearchResults:foundMatches];
        foundMatches = nil;
        
        //Back on Main Thread
        dispatch_async(dispatch_get_main_queue(), ^{
            //Reload Search Results Table
            [self.tblSearchResults reloadData];
            //Animate loading finished and show results
            [UIView animateWithDuration:0.2 animations:^{
                [self.tblSearchResults setAlpha:1.0f];
                [self.aiSearching setAlpha:0.0f];
            } completion:^(BOOL finished) {
                [self.tblSearchResults setUserInteractionEnabled:YES];
            }];
        });
        
    });
}

#pragma mark - Table View Setup

-(void)configureTableStyle{
    //Configure Table style removing empty rows and setting a row height
    self.tblSearchResults.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tblSearchResults setRowHeight:60.0f];
    
    self.tblFavouriteLocations.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tblFavouriteLocations setRowHeight:60.0f];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Custom cell so remove inset to improve display
    
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //Only one section required
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //Number of rows will be the number of search results or favourite locations based on table
    return tableView == self.tblSearchResults ? [self.searchResults count] : [[[FavouritesManager sharedInstance] favouriteLocationIDs] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.tblSearchResults){
        //Search Results Cell
        
        //Cell identifier
        static NSString *celIdentifier = @"searchResultCell";
        //Create custom view table cell
        LocationTableViewCell *cell = (LocationTableViewCell *)[tableView dequeueReusableCellWithIdentifier:celIdentifier];
        //Load cell from NIB if doesn't exist
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:nibLocationTableViewCell owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        //Find the approprate location obect
        Location *loc = [self.searchResults objectAtIndex:indexPath.row];
        //Set Value
        cell.lblName.text = loc.name;
        cell.lblCountry.text = loc.countryCode;
        //Set Favourite Indicator
        cell.imageView.image = [[FavouritesManager sharedInstance] isFavourite:loc.identifier] ? [UIImage imageNamed:@"FavouriteIconSelected"] : [UIImage imageNamed:@"FavouriteIcon"];
        [cell.imageView setTintColor:[UIColor whiteColor]];
        //Set Button Action
        [cell.btnFavourite addTarget:self action:@selector(searchResultFavouriteTapped:) forControlEvents:UIControlEventTouchUpInside];
        //Set Selection Style
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //Return Cell
        return cell;
    }else{
        //Favourite Locations Cell
        
        //Cell identifier
        static NSString *celIdentifier = @"favouriteLocationCell";
        //Create custom view table cell
        LocationTableViewCell *cell = (LocationTableViewCell *)[tableView dequeueReusableCellWithIdentifier:celIdentifier];
        //Load cell from NIB if doesn't exist
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:nibLocationTableViewCell owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        //Find the approprate location obect
        NSNumber *identifier = [[[FavouritesManager sharedInstance] favouriteLocationIDs] objectAtIndex:indexPath.row];
        Location *loc = [[LocationsManager sharedInstance] locationForIdentifier:identifier];
        //Set Value
        cell.lblName.text = loc.name;
        cell.lblCountry.text = loc.countryCode;
        //Set Favourite Indicator
        cell.imageView.image = [UIImage imageNamed:@"FavouriteIconSelected"];
        [cell.imageView setTintColor:[UIColor whiteColor]];
        //Set Button Action
        [cell.btnFavourite addTarget:self action:@selector(curentFavouriteTapped:) forControlEvents:UIControlEventTouchUpInside];
        //Set Selection Style
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //Return Cell
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //ShowLoadingView
    [UIView animateWithDuration:0.3 animations:^{
        [self.vwLoading setAlpha:1.0f];
    } completion:nil];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    Location *loc;
    if (tableView == self.tblSearchResults){
        loc = [self.searchResults objectAtIndex:indexPath.row];
    }else{
        NSNumber *identifier = [[[FavouritesManager sharedInstance] favouriteLocationIDs] objectAtIndex:indexPath.row];
        loc = [[LocationsManager sharedInstance] locationForIdentifier:identifier];
    }
    
    //Get initial current forecast data
    [OpenWeatherServiceManager getCurrentForecastForIDLocation:loc.identifier withCompletion:^(CurrentForecast * _Nonnull forecast, NSError * _Nonnull error) {
        if (error){
            //Error Occured Show UI
            UIAlertController * errorAlert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Could not load data, please try again." preferredStyle:UIAlertControllerStyleAlert];
            //Add button for cancel
            UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
            [errorAlert addAction:cancel];
            //Show Alert
            [self presentViewController:errorAlert animated:YES completion:nil];
        }else{
            //Create Forecast View
            LocationForecastViewController *forecastView = [[LocationForecastViewController alloc] init];
            forecastView.location = loc;
            forecastView.currentForecast = forecast;
            //Back on Main Thread Push Controller
            dispatch_async(dispatch_get_main_queue(), ^{
                //Push
                [UIView animateWithDuration:0.3 animations:^{
                    [self.vwLoading setAlpha:0.0f];
                } completion:nil];
                [self.navigationController pushViewController:forecastView animated:YES];
            });
        }
    }];
    
}

#pragma mark - Manage Favourites

-(IBAction)searchResultFavouriteTapped:(id)sender{
    //Toggle search result as favourite
    [self toggleFavourite:sender onTable:self.tblSearchResults];
}

-(IBAction)curentFavouriteTapped:(id)sender{
    //Toggle current favourite as favourite
    [self toggleFavourite:sender onTable:self.tblFavouriteLocations];
    [self configureViewState];
}


- (void)toggleFavourite:(id)sender onTable:(UITableView *)tableView {
    
    //Get point of origin
    CGPoint origin = [sender convertPoint:CGPointZero toView:tableView];
    NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:origin];
    
    //Find the approprate location obect
    Location *loc;
    if (tableView == self.tblSearchResults){
        loc = [self.searchResults objectAtIndex:indexPath.row];
    }else{
        NSNumber *identifier = [[[FavouritesManager sharedInstance] favouriteLocationIDs] objectAtIndex:indexPath.row];
        loc = [[LocationsManager sharedInstance] locationForIdentifier:identifier];
    }
    
    //Remove / Add Favourite
    [[FavouritesManager sharedInstance] toggleFavourite:loc.identifier];
    
    //Update Display
    [tableView reloadData];
}



@end

