//
//  LocationForecastViewController.m
//  Weather Alert
//
//  Created by Adam Stone on 07/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import "LocationForecastViewController.h"
#import "UIimage+Rotate.h"
#import "NibNames.h"
#import "MainConfiguration.h"
#import "ForecastTableViewCell.h"
#import "ForecastGroupHeaderCell.h"

@interface LocationForecastViewController ()

@end

@implementation LocationForecastViewController

@synthesize lblCurrentSpeed;
@synthesize lblLocationName;
@synthesize lblCardinalDirection;
@synthesize vwLocationName;
@synthesize imgCardinalDirection;
@synthesize tblFullForecast;
@synthesize imgLocationIcon;
@synthesize btnBack;
@synthesize aiLoading;

#pragma mark - View Load

- (id)init
{
    self = [self initWithNibName:nibLocationForecastView bundle:nil];
    
    if (self) {
        [self setTitle:@""];
    };
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureTableStyle];
    [self configureViewStyle];
    [self loadCurrentForecast];
    [self configureForecastManager];
}

#pragma mark - Table View Setup

-(void)configureTableStyle{
    //Configure Table style removing empty rows and setting a row height
    self.tblFullForecast.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tblFullForecast setRowHeight:60.0f];
}

#pragma mark - View Style setup
- (void)configureViewStyle {
    [[self.vwLocationName layer] setCornerRadius:8.0f];
    [self.imgLocationIcon setTintColor:[UIColor whiteColor]];
}


#pragma mark - Load Current Forecast Data

-(void)loadCurrentForecast {
    
   
    //Set to label
    self.lblCurrentSpeed.attributedText = [self getSpeedAttributedStringForSpeed:self.currentForecast.speed withTitleSize:100.0f andSubTitleSize:36.0f];
    
    //Cardinal Direction
    self.lblCardinalDirection.text = self.currentForecast.cardinalDirection;
    
    //Location Name
    self.lblLocationName.text = [NSString stringWithFormat:@"%@, %@", self.location.name, self.location.countryCode];
    
    //Cardinal Direction Image
    self.imgCardinalDirection.image = [[UIImage imageNamed:@"DirectionPointer"] rotateByDegrees:self.currentForecast.degrees];
    [self.imgCardinalDirection setTintColor:[UIColor colorWithRed:0.55 green:0.90 blue:0.90 alpha:1.00]];
}

-(NSAttributedString *)getSpeedAttributedStringForSpeed:(double)speed withTitleSize:(float)titleSize andSubTitleSize:(float)subTitleSize{
    
    //Wind Speed Details
    NSString *windSpeed = [NSString stringWithFormat:@"%.1f",speed];
    NSString *windUnit = speedUnit;
    NSString *windDetails = [NSString stringWithFormat:@"%@%@", windSpeed, windUnit];
    
    //Set Attributes
    NSDictionary *speedAttribs = @{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"KohinoorBangla-Semibold" size:titleSize]};
    NSDictionary *unitAttribs = @{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"KohinoorBangla-Light" size:subTitleSize]};
    
    //Get Range
    NSRange speedRange = [windDetails rangeOfString:windSpeed];
    NSRange unitRange = [windDetails rangeOfString:windUnit];
    
    //Create Attributed String
    NSMutableAttributedString *attrWindSpeed = [[NSMutableAttributedString alloc] initWithString:windDetails attributes:nil];
    [attrWindSpeed setAttributes:speedAttribs range:speedRange];
    [attrWindSpeed setAttributes:unitAttribs range:unitRange];
    
    //Return String
    return attrWindSpeed;
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Custom cell so remove inset to improve display
    
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //Only one section required
    return self.forecastManager ? [self.forecastManager.groupedForecasts count] : 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //Number of rows will be the number of ratings available on this scheme
    return self.forecastManager ? [[[self.forecastManager.groupedForecasts objectAtIndex:section] objectForKey:kDateGroupForecasts] count] : 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    //Forecast Group Header Cell
    
    //Cell identifier
    static NSString *celIdentifier = @"forecastGroupHeaderCell";
    
    //Create custom table cell
    ForecastGroupHeaderCell *cell = (ForecastGroupHeaderCell *)[tableView dequeueReusableCellWithIdentifier:celIdentifier];
    
    //Load cell from NIB if doesn't exist
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:nibForecastGroupHeaderCell owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    //Find the approprate forecast group obect
    NSString *groupTitle =[[self.forecastManager.groupedForecasts objectAtIndex:section] objectForKey:kDateGroupTitle];
    
    //Set Label
    cell.lblForecastGroupTitle.text = groupTitle;
    
    return cell;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //Forecast Cell
    
    //Cell identifier
    static NSString *celIdentifier = @"forecastDetailsCell";
    //Create custom table cell
    ForecastTableViewCell *cell = (ForecastTableViewCell *)[tableView dequeueReusableCellWithIdentifier:celIdentifier];
    //Load cell from NIB if doesn't exist
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:nibForecastTableViewCell owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    //Find the approprate forecast obect
    FullForecast *forecast = [[[self.forecastManager.groupedForecasts objectAtIndex:indexPath.section] objectForKey:kDateGroupForecasts] objectAtIndex:indexPath.row];
    //Set Time Value
    NSDateFormatter *timeOnlyFormatter = [[NSDateFormatter alloc] init];
    [timeOnlyFormatter setDateFormat:@"HH:mm"];
    cell.lblTime.text = [timeOnlyFormatter stringFromDate:forecast.dateFrom];
    cell.lblWindSpeed.attributedText =  [self getSpeedAttributedStringForSpeed:forecast.speed withTitleSize:36.0f andSubTitleSize:18.0f];
    cell.lblCardinalDirection.text = forecast.cardinalDirection;
    //Set Selection Style
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //Return Cell
    return cell;
}

#pragma mark - Forecast Manager
- (void)configureForecastManager{
    //Configure Forecast Manager
    self.forecastManager = [[FullForecastManager alloc] init];
    [self.forecastManager setFullForecastDelegate:self];
    //Begin loading data
    [self.forecastManager loadFullForecastForLocation:self.location];
}

#pragma mark - Forecast Manager Delegate Methods

- (void) didFailToLoadFullForecast {
    //Error Occured Show UI
    UIAlertController * errorAlert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Could not load data, please try again." preferredStyle:UIAlertControllerStyleAlert];
    //Add button for cancel
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    [errorAlert addAction:cancel];
    //Show Alert
    [self presentViewController:errorAlert animated:YES completion:nil];
    //Animate loading finished and show nothing due to error
    [UIView animateWithDuration:0.2 animations:^{
        [self.tblFullForecast setAlpha:0.0f];
    } completion:nil];
}

- (void) didLoadFullForecast{
    //Back on Main Thread
    dispatch_async(dispatch_get_main_queue(), ^{
        
        //Forecast Manager has data so reload table
        [self.tblFullForecast reloadData];
        
        //Animate loading finished and show results
        [UIView animateWithDuration:0.2 animations:^{
            [self.tblFullForecast setAlpha:1.0f];
            [self.aiLoading setAlpha:0.0f];
        } completion:nil];
        
    });
}
#pragma mark - Navigation

- (IBAction)btnBackTapped:(id)sender {
    //Go Back
    [self.navigationController popViewControllerAnimated:YES];
}
@end
