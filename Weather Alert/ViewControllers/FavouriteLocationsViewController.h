//
//  FavouriteLocationsViewController.h
//  Weather Alert
//
//  Created by Adam Stone on 06/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationsManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface FavouriteLocationsViewController : UIViewController<UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>

//UI Elements
@property (weak, nonatomic) IBOutlet UIView *vwEmptyData;
@property (weak, nonatomic) IBOutlet UIView *vwHeader;
@property (weak, nonatomic) IBOutlet UITextField *txtSearchBar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgEmptyDataIcon;
@property (weak, nonatomic) IBOutlet UIButton *btnCancelSearch;
@property (weak, nonatomic) IBOutlet UITableView *tblSearchResults;
@property (weak, nonatomic) IBOutlet UITableView *tblFavouriteLocations;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *aiSearching;
@property (weak, nonatomic) IBOutlet UIView *vwLoading;
@property (weak, nonatomic) IBOutlet UIView *vwLoadingInner;

//Objects
@property (weak, nonatomic) NSTimer *searchDelayTimer;

//Data Store
@property (strong, nonatomic, nullable) NSArray<Location *> *searchResults;

//Constraints
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cHeaderHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cSearchBarTrailing;

//Events
- (IBAction)txtSearchBarEditingDidBegin:(id)sender;
- (IBAction)txtSearchBarEditingChanged:(id)sender;
- (IBAction)btnCancelSearchTapped:(id)sender;


@end

NS_ASSUME_NONNULL_END
