//
//  ForecastGroupHeaderCell.h
//  Weather Alert
//
//  Created by Adam Stone on 07/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ForecastGroupHeaderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblForecastGroupTitle;

@end

NS_ASSUME_NONNULL_END
