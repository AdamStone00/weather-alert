# Weather Alert
Implementation of an application that allows the user to check the wind conditions and forecast at locations around the world.

##### Language Choice
- I have chosen Objective-C for this build based on the limited time factor and it being the quickest / most familiar language to me. Hopefully this still helps demonstrate a full understanding of the iOS SDK and it's frameworks.

##### Third party libraries
- There are no third party libraries used within this project. Whilst some tasks might have been achieved easily with a third party library I wanted to show that I still had understanding of the underlying framework.

##### Missed Features / Caveats
- App does not support light or dark mode, this would have been nice to do but for speed I have just set it as one style.
- Error feedback within the UI is currently poor, simple messages to say something has gone wrong, this in a real world solution wouldn't be sufficient.
- App has been designed for iPhone predominantly.
- Different screen sizes have been tested in the simulator but only tested physically on 6s Plus and X.
- Target iOS version is 12.0 purely being the latest major release.
- Examples of Unit Testing have been included but this is not a full implementation, again this was purely down to time.

