//
//  LocationTests.m
//  Weather AlertTests
//
//  Created by Adam Stone on 06/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Location.h"

@interface LocationTests : XCTestCase
    @property (nonatomic) NSDictionary *locDict;
    @property (nonatomic) Location *loc;
@end

@implementation LocationTests

- (void)setUp {
    self.locDict = @{@"id" : @1, @"name" : @"Test Location", @"country" : @"test"};
}

- (void)tearDown {
    self.locDict = nil;
    self.loc = nil;
}

- (void)testLocationCreated {
    //Given
    [self givenLocationCreatedFromDict];
    
    //Then
    XCTAssertNotNil(self.loc);
}

-(void)testLocationNameStored {
    //Given
    [self givenLocationCreatedFromDict];
    
    //Then
    XCTAssertTrue([self.loc.name isEqualToString:@"Test Location"]);
}

-(void)testLocationIDStored {
    //Given
    [self givenLocationCreatedFromDict];
    
    //Then
    XCTAssertEqual(1, [self.loc.identifier intValue]);
}

-(void)testLocationCountryStored {
    //Given
    [self givenLocationCreatedFromDict];
    
    //Then
    XCTAssertTrue([self.loc.countryCode isEqualToString:@"test"]);
}

-(void)givenLocationCreatedFromDict{
    self.loc = [[Location alloc] initWithDictionary:self.locDict];
}



@end
