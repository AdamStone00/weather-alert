//
//  FullForecastTests.m
//  Weather AlertTests
//
//  Created by Adam Stone on 07/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FullForecast.h"

@interface FullForecastTests : XCTestCase

@property (nonatomic) NSDictionary *fullForecastDict;
@property (nonatomic) FullForecast *fullForecast;

@end

@implementation FullForecastTests

- (void)setUp {
    NSDictionary* dict = @{@"speed" : @3.25, @"deg" : @287.822};
    self.fullForecastDict = @{@"dt": @946720800, @"wind" : dict};
}

- (void)tearDown {
    self.fullForecast = nil;
    self.fullForecastDict = nil;
}

- (void)testFullForecastCreated {
    //Given
    [self givenFullForecastCreatedFromDict];
    
    //Then
    XCTAssertNotNil(self.fullForecast);
}

-(void)testFullForecastSpeedStored {
    //Given
    [self givenFullForecastCreatedFromDict];
    
    //Then
    XCTAssertEqual(3.25, self.fullForecast.speed);
}

-(void)testFullForecastDegreesStored {
    //Given
    [self givenFullForecastCreatedFromDict];
    
    //Then
    XCTAssertEqual(287.822, self.fullForecast.degrees);
}

-(void)testCorrectCardinalDirectionCalculated {
    //Given
    [self givenFullForecastCreatedFromDict];
    
    //Then
    XCTAssertTrue([self.fullForecast.cardinalDirection isEqualToString:@"WNW"]);
}

-(void)testFullForecastDateStored {
    //Given
    [self givenFullForecastCreatedFromDict];
    
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:1];
    [comps setMonth:1];
    [comps setYear:2000];
    [comps setHour:10];
    [comps setMinute:0];
    [comps setSecond:0];
    NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:comps];
    
    //Then
    XCTAssertEqualWithAccuracy([self.fullForecast.dateFrom timeIntervalSinceReferenceDate], [date timeIntervalSinceReferenceDate], 0.001);
}

-(void)givenFullForecastCreatedFromDict{
    self.fullForecast = [[FullForecast alloc] initWithDictionary:self.fullForecastDict];
}


@end
