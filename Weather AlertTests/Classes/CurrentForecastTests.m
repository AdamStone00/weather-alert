//
//  CurrentForecastTests.m
//  Weather AlertTests
//
//  Created by Adam Stone on 06/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CurrentForecast.h"

@interface CurrentForecastTests : XCTestCase

    @property (nonatomic) NSDictionary *forecastDict;
    @property (nonatomic) CurrentForecast *forecast;

@end

@implementation CurrentForecastTests

- (void)setUp {
    
    NSDictionary* dict = @{@"speed" : @3.25, @"deg" : @287.822};
    self.forecastDict = @{@"wind" : dict};
}

- (void)tearDown {
    self.forecast = nil;
    self.forecastDict = nil;
}

- (void)testForecastCreated {
    //Given
    [self givenForecastCreatedFromDict];
    
    //Then
    XCTAssertNotNil(self.forecast);
}

-(void)testForecastSpeedStored {
    //Given
    [self givenForecastCreatedFromDict];
    
    //Then
    XCTAssertEqual(3.25, self.forecast.speed);
}

-(void)testForecastDegreesStored {
    //Given
    [self givenForecastCreatedFromDict];
    
    //Then
    XCTAssertEqual(287.822, self.forecast.degrees);
}

-(void)testCorrectCardinalDirectionCalculated {
    //Given
    [self givenForecastCreatedFromDict];
    
    //Then
    XCTAssertTrue([self.forecast.cardinalDirection isEqualToString:@"WNW"]);
}

-(void)givenForecastCreatedFromDict{
    self.forecast = [[CurrentForecast alloc] initWithDictionary:self.forecastDict];
}

@end
