//
//  DataConverterTests.m
//  Weather AlertTests
//
//  Created by Adam Stone on 07/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DataConverters.h"

@interface DataConverterTests : XCTestCase
 @property (nonatomic) NSDictionary *testDict;
@end

@implementation DataConverterTests

- (void)setUp {
     self.testDict = @{@"propertyOne" : @1, @"propertyTwo" : @"test"};
}

- (void)tearDown {
    self.testDict = nil;
}

- (void)testConverterReturnsNilWhenObjectDoesntExist {
    //When
    NSString *expectedNil = [DataConverters objectOrNilForKey:@"fakeProperty" fromDictionary:self.testDict];
    
    //Then
    XCTAssertNil(expectedNil);
}

- (void)testConverterUnpacksString {
    //When
    NSString *expectedString = [DataConverters objectOrNilForKey:@"propertyTwo" fromDictionary:self.testDict];
    
    //Then
    XCTAssertTrue([expectedString isEqualToString:@"test"]);
}

- (void)testConverterUnpacksNumber {
    //When
    NSNumber *expectedNumber = [DataConverters objectOrNilForKey:@"propertyOne" fromDictionary:self.testDict];
    
    //Then
    XCTAssertEqual(expectedNumber, [NSNumber numberWithInt:1]);
}


@end
