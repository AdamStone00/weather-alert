//
//  LocationsManagerTests.m
//  Weather AlertTests
//
//  Created by Adam Stone on 06/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "LocationsManager.h"

@interface LocationsManagerTests : XCTestCase

@end

@implementation LocationsManagerTests

- (void)testLocationsManagerHasLocations {
    //Then
    XCTAssertNotNil([[LocationsManager sharedInstance] locations]);
}

-(void)testLocationsManagerCanFindLocation {
    //Given
    NSNumber *testIdentifier = [NSNumber numberWithInt:4298960];
    
    //When
    Location *loc = [[LocationsManager sharedInstance] locationForIdentifier:testIdentifier];
    
    //Then
    XCTAssertNotNil(loc);
}

-(void)testLocationsManagerCanFindCorrectLocation {
    //Given
    NSNumber *testIdentifier = [NSNumber numberWithInt:4298960];
    NSString *expectedLocatioName = @"London";
    
    //When
    Location *loc = [[LocationsManager sharedInstance] locationForIdentifier:testIdentifier];
    
    //Then
    XCTAssertTrue([expectedLocatioName isEqualToString:loc.name]);
}


@end
