//
//  FavouritesManagerTests.m
//  Weather AlertTests
//
//  Created by Adam Stone on 06/08/2019.
//  Copyright © 2019 Adam Stone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FavouritesManager.h"

@interface FavouritesManagerTests : XCTestCase

    @property (nonatomic) Location *loc;

@end

@implementation FavouritesManagerTests


- (void)setUp {
    NSDictionary *locDict = @{@"id" : @1, @"name" : @"Test Lcoation", @"country" : @"test"};
    self.loc = [[Location alloc] initWithDictionary:locDict];
}

- (void)tearDown {
    self.loc = nil;
}

- (void)testFavouritesManagerCanIdentifyFavourite {
    //Then
    XCTAssertFalse([[FavouritesManager sharedInstance] isFavourite:self.loc.identifier]);
}

- (void)testFavouritesManagerCanAddFavourite {
    //Given
    [[FavouritesManager sharedInstance] toggleFavourite:self.loc.identifier];
    
    //Then
    XCTAssertTrue([[FavouritesManager sharedInstance] isFavourite:self.loc.identifier]);
}

- (void)testFavouritesManagerCanRemoveFavourite {
    //Given
    [[FavouritesManager sharedInstance] toggleFavourite:self.loc.identifier];
    
    //Then
    XCTAssertTrue([[FavouritesManager sharedInstance] isFavourite:self.loc.identifier]);
}


@end
